import upload from '@/utils/upload'
import request from '@/utils/request'



// 查询用户个人信息
export function getToDoList() {
  return request({
    url: '/system/mobile/toDoList',
    method: 'get'
  })
}


