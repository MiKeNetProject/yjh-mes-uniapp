import request from '@/utils/request'

// 查询执行单据数量
export function executeBillCount(query) {
  return request({
    url: "/statement/mobileWork/executeBillCount",
    method: "get",
    params: query,
  });
}